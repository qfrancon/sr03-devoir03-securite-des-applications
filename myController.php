<?php
  require_once('include.php');
  require_once('myModel.php');
  
  session_start();
  
  // URL de redirection par défaut (si pas d'action ou action non reconnue)
  $url_redirect = "index.php";
  
  if (isset($_REQUEST['action'])) {
  
      if ($_REQUEST['action'] == 'authenticate') {
          /* ======== AUTHENT ======== */
          if (ipIsBanned($_SERVER['REMOTE_ADDR'])){
              // cette IP est bloquée
              $url_redirect = "vw_login.php?ipbanned";

          } else if (!isset($_REQUEST['login']) || !isset($_REQUEST['mdp']) || $_REQUEST['login'] == "" || $_REQUEST['mdp'] == "") {
              // manque login ou mot de passe
              $url_redirect = "vw_login.php?nullvalue";
              
          } else {
              $car_interdits = array("'", "\"", ";","%"); // une liste de caractères que je choisis d'interdire
              $utilisateur = findUserByLoginPwd(str_replace($car_interdits, "", $_REQUEST['login']), str_replace($car_interdits, "", $_REQUEST['mdp']), $_SERVER['REMOTE_ADDR']);

              if ($utilisateur == false) {
                // echec authentification
                $url_redirect = "vw_login.php?badvalue";
                
              } else {
                  $rand = substr(md5(microtime()),rand(0,26),5);
                  mail("testsr03sr03@gmail.com", "Authenticate", "$rand", "Authenticate");
                  $_SESSION["login"] = $_REQUEST['login'];
                  $_SESSION["mdp"] = $_REQUEST['mdp'];
                      // authentification réussie
                $_SESSION["doubleAuth"] =  $rand;
                $url_redirect = "vw_confirm_mail_code.php";
              }
          }

          
      } else if ($_REQUEST['action'] == 'disconnect') {
          /* ======== DISCONNECT ======== */
          // unset($_SESSION["connected_user"]);
          session_unset() ;
          $url_redirect = "vw_login.php?disconnect";

          
      } else if ($_REQUEST['action'] == 'transfert') {
          /* ======== TRANSFERT ======== */
          if (!isset($_REQUEST['mytoken']) || $_REQUEST['mytoken'] != $_SESSION['mytoken']) {
              // echec vérification du token (ex : attaque CSRF)
              $url_redirect = "vw_moncompte.php?err_token";
          } else {
              if (is_numeric ($_REQUEST['montant'])) {
                  transfert($_REQUEST['destination'],$_SESSION["connected_user"]["numero_compte"], $_REQUEST['montant']);
                  $_SESSION["connected_user"]["solde_compte"] = $_SESSION["connected_user"]["solde_compte"] -  $_REQUEST['montant'];
                  $url_redirect = "vw_moncompte.php?trf_ok";

              } else {
                  $url_redirect = "vw_moncompte.php?bad_mt=".$_REQUEST['montant'];
              }
          }


      } else if ($_REQUEST['action'] == 'sendmsg') {
          /* ======== MESSAGE ======== */
          addMessage($_REQUEST['to'],$_SESSION["connected_user"]["id_user"],$_REQUEST['sujet'],$_REQUEST['corps']);
          $url_redirect = "vw_moncompte.php?msg_ok";

              
      } else if ($_REQUEST['action'] == 'msglist') {
          /* ======== MESSAGE ======== */
          $_SESSION['messagesRecus'] = findMessagesInbox($_SESSION["connected_user"]["id_user"]);
          $url_redirect = "vw_messagerie.php";
              
      } else if ($_REQUEST['action'] == 'virement') {
          $url_redirect = "vw_virement.php";

      } else if ($_REQUEST['action'] == 'admin') {
          $url_redirect = "vw_gerer_clients.php";

      } else if ($_REQUEST['action'] == 'selectClient') {
          $_SESSION['selectedUser'] =  selectClient($_REQUEST['to'],$_SESSION["connected_user"]["id_user"], $_SESSION["connected_user"]["profil_user"]);
          $url_redirect = "vw_gerer_clients.php";

      } else if ($_REQUEST['action'] == 'confirmPwd') {
          $_SESSION['prevMontant'] = $_REQUEST['montant'];
          $_SESSION['prevDestination'] = $_REQUEST['destination'];
          $url_redirect = "confirmPwd.php";

      } else if ($_REQUEST['action'] == 'confirm_code') {
          if($_SESSION['doubleAuth'] == $_REQUEST['code']){
              $car_interdits = array("'", "\"", ";","%"); // une liste de caractères que je choisis d'interdire
              $utilisateur = findUserByLoginPwd(str_replace($car_interdits, "", $_SESSION['login']), str_replace($car_interdits, "", $_SESSION['mdp']), $_SERVER['REMOTE_ADDR']);
              $_SESSION["connected_user"] = $utilisateur;
              $_SESSION["listeUsers"] = findAllUsers();
              $url_redirect = "vw_moncompte.php";
          }

      } else if ($_REQUEST['action'] == 'confirmPwd2') {

          if (ipIsBanned($_SERVER['REMOTE_ADDR'])){
              $url_redirect = "vw_login.php?ipbanned";

          } else if (!isset($_REQUEST['login']) || !isset($_REQUEST['mdp']) || $_REQUEST['login'] == "" || $_REQUEST['mdp'] == "") {
              // manque login ou mot de passe
              $url_redirect = "vw_moncompte.php?nullvalue";

          } else {
              $car_interdits = array("'", "\"", ";","%"); // une liste de caractères que je choisis d'interdire
              $utilisateur = findUserByLoginPwd(str_replace($car_interdits, "", $_REQUEST['login']), str_replace($car_interdits, "", $_REQUEST['mdp']), $_SERVER['REMOTE_ADDR']);

              if ($utilisateur == false) {
                  // echec authentification
                  $url_redirect = "vw_moncompte.php?badvalue";

              } else {
                      if (is_numeric ($_SESSION['prevMontant']) && ($_SESSION['prevMontant']) > 0 && ($_SESSION['prevMontant']) <= $_SESSION["connected_user"]["solde_compte"]) {
                          transfert($_SESSION['prevDestination'],$_SESSION["connected_user"]["numero_compte"], $_SESSION['prevMontant']);
                          $_SESSION["connected_user"]["solde_compte"] = $_SESSION["connected_user"]["solde_compte"] -  $_SESSION['prevMontant'];
                          $url_redirect = "vw_moncompte.php?trf_ok";

                      } else {
                          $url_redirect = "vw_moncompte.php?bad_mt=".$_SESSION['prevMontant'];
                      }
                  }
              }
          }
      }
  
  header("Location: $url_redirect");

?>
