# SR03-Devoir03-Sécurité des Applications

Pour setup le projet il faut :

0. mettre le dossier git dans le www de votre WAMP ou indiquer et créer un env virtuel avec le path vers le dossier git téléchargé

1. régler les paramètres du config.php dans config pour convenir à votre base de donnée mySql

2. se rendre dans le php.ini de votre serveur WAMP, (clique gauche sur l'icone en barre des tâches, php, ouvrir php.ini ) 
trouver la ligne : sendmail_path puis indiquer dans ce sendmail_path = "le chemin absolu vers le dossier sendmail compris dans les fichiers du git et finalement le /sendmail.exe au bout de ce chemin absolu"

Maintenant redémarrez les services WAMP avec clique gauche => services redémarrer.

3. Maintenant, vous pouvez utiliser le script Sql dans sqlTables.txt pour créer la database avec des utilisateurs pré inscrits

4. adresse de login une fois toutes les étapes ci-dessus faites : 

http://localhost/sr03p21/vw_login.php (si vous êtes avec le dossier git dans votre www wamp )

liste des utilisateurs : 

-user1
-user2
-user3
-employe1
-employe2
-employe3

tous les mots de passes sont : 
- test

l'adresse mail est codé en dur ( il serait facile de la récupérer dans la database mais par soucis d'efficacité pour la correction :

l'adresse mail utilisée pour tout est : 
- testsr03sr03@gmail.com 
password :
- sr03sr03&1

Si vous testez le projet vous aurez besoin de vous rendre sur gmail pour récupérer le code de confirmation de connexion.




