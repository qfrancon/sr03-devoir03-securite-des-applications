<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Connexion</title>
    <link rel="stylesheet" type="text/css" media="all"  href="css/mystyle.css" />
    <script type="text/javascript" src="js/myscript.js"></script>
</head>
<body>
<header>
    <h1>Connexion</h1>
</header>

<section>
    <div class="login-page">
        <div class="form">
            <form method="POST" action="myController.php">
                <input type="hidden" name="action" value="confirm_code">
                <input type="code" id="code" name="code" placeholder="code"/>
                <button>login</button>
            </form>
        </div>
    </div>
</section>

