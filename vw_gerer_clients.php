<?php
require_once('include.php');

session_start();

if(!isset($_SESSION["connected_user"]) || $_SESSION["connected_user"] == "" || $_SESSION["connected_user"]["profil_user"] != "EMPLOYE") {
    // utilisateur non connecté
    header('Location: vw_login.php');
    exit();
}


?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Messages</title>
    <link rel="stylesheet" type="text/css" media="all"  href="css/mystyle.css" />
</head>

<body>
<header>
    <form method="POST" action="myController.php">
        <button class="btn-back form-btn">Retour</button>
    </form>
    <form method="POST" action="myController.php">
        <input type="hidden" name="action" value="disconnect">
        <button class="btn-logout form-btn">Déconnexion</button>
    </form>

    <h2><?php echo $_SESSION["connected_user"]["prenom"];?> <?php echo $_SESSION["connected_user"]["nom"];?> - Messages reçus</h2>
</header>

    </article>
    <article>
        <form method="POST" action="myController.php">
            <input type="hidden" name="action" value="selectClient">
            <div class="fieldset">
                <div class="fieldset_label">
                    <span>Administrer un client</span>
                </div>
                <div class="field">
                    <label>Client à gérer : </label>
                    <select name="to">
                        <?php
                        foreach ($_SESSION['listeUsers'] as $id => $user) {
                            if($user["profil_user"] != "EMPLOYE" && $_SESSION["connected_user"]['id_user'] != $user['id_user']){
                                echo '<option value="'.$id.'">'.$user['nom'].' '.$user['prenom'].'</option>';
                            }
                        }
                        ?>
                    </select>
                </div>
                <button class="form-btn">Sélectionner</button>
        </form>
            </div>
            <article>
                <form method="POST" action="myController.php">
                    <input type="hidden" name="action" value="confirmPwd">
                    <div class="fieldset">
                        <div class="fieldset_label">
                            <span>Transférer de l'argent</span>
                        </div>
                        <div class="field">
                            <label>N° compte destinataire : </label><input type="text" size="20" name="destination" value=<?php if($_SESSION["selectedUser"]["numero_compte"]){ echo $_SESSION["selectedUser"]["numero_compte"];} ?>>
                        </div>
                        <div class="field">
                            <label>Montant à transférer : </label><input type="text" size="10" name="montant">
                        </div>
                        <button class="form-btn">Transférer</button>
                        <?php
                        if (isset($_REQUEST["err_token"])) {
                            echo '<p>Echec virement : le contrôle d\'intégrité a échoué.</p>';
                        }
                        if (isset($_REQUEST["trf_ok"])) {
                            echo '<p>Virement effectué avec succès.</p>';
                        }
                        if (isset($_REQUEST["bad_mt"])) {
                            echo '<p>Le montant saisi est incorrect : '.htmlentities($_REQUEST["bad_mt"], ENT_QUOTES).'</p>';
                        }
                        ?>
                    </div>
                </form>
            </article>
            <article>
                <div class="fieldset">
                    <div class="fieldset_label">
                        <span>Informations sur le client</span>
                    </div>
                    <div class="field">
                        <label>Login : </label><span><?php if($_SESSION["selectedUser"]["login"]){echo $_SESSION["selectedUser"]["login"];}?></span>
                    </div>
                    <div class="field">
                        <label>Profil : </label><span><?php if($_SESSION["selectedUser"]["profil_user"]){echo $_SESSION["selectedUser"]["profil_user"];}?></span>
                    </div>
                </div>
            </article>

            <article>
                <div class="fieldset">
                    <div class="fieldset_label">
                        <span>Compte du client</span>
                    </div>
                    <div class="field">
                        <label>N° compte : </label><span><?php if($_SESSION["selectedUser"]["numero_compte"]){ echo $_SESSION["selectedUser"]["numero_compte"];} ?></span>
                    </div>
                    <div class="field">
                        <label>Solde : </label><span><?php if($_SESSION["selectedUser"]["solde_compte"]){echo $_SESSION["selectedUser"]["solde_compte"];}?></span>
                    </div>
                </div>
            </article>
        </form>
    </article>
</body>
</html>
