<?php
require_once('include.php');

session_start();

if(!isset($_SESSION["connected_user"]) || $_SESSION["connected_user"] == "" || $_SESSION["connected_user"]["profil_user"] != "EMPLOYE") {
    // utilisateur non connecté
    header('Location: vw_login.php');
    exit();
}

$mytoken = bin2hex(random_bytes(128)); // token qui va servir à prévenir des attaques CSRF
$_SESSION["mytoken"] = $mytoken;
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Connexion</title>
    <link rel="stylesheet" type="text/css" media="all"  href="css/mystyle.css" />
    <script type="text/javascript" src="js/myscript.js"></script>
</head>
<body>
<header>
    <h1>Connexion</h1>
</header>

<section>
    <div class="login-page">
        <div class="form">
            <form method="POST" action="myController.php">
                <input type="hidden" name="action" value="confirmPwd2">
                <input type="text" id="login" name="login" placeholder="login" value="<?php echo $_SESSION["connected_user"]["login"];?>"/>
                <input type="password" id="mdp" name="mdp" placeholder="mot de passe"/>
                <button>login</button>
            </form>
        </div>
    </div>

    <?php
    if (isset($_REQUEST["nullvalue"])) {
        echo '<p class="errmsg">Merci de saisir votre login et votre mot de passe pour valider la transaction</p>';
    } else if (isset($_REQUEST["badvalue"])) {
        echo '<p class="errmsg">Votre login/mot de passe est incorrect</p>';
    } else if (isset($_REQUEST["ipbanned"])) {
        echo '<p class="errmsg">Nombre de tentatives maximal atteint ! Contactez votre gestionnaire.</p>';
    } else if (isset($_REQUEST["disconnect"])) {
        echo '<p>Vous avez bien été déconnecté.</p>';
    }

    ?>
</section>

</body>
</html>
